#!/usr/bin/env python

import argparse
import logging
from os import access, F_OK, R_OK
from os.path import split
from sys import stdout

from rdflib import Graph
from rdflib.util import guess_format

from kgfpm.frequent_pattern_miner import frequent_pattern_miner


def is_readable(filename):
    path = split(filename)[0]
    if not access(path, F_OK):
        raise OSError(":: Path does not exist: {}".format(path))
    elif not access(path, R_OK):
        raise OSError(":: Path not readable by user: {}".format(path))

    return True

def init_logger(verbose=0):
    if verbose == 0:
        return

    logging.basicConfig(format='[%(asctime)s] %(module)s/%(funcName)s | %(levelname)s: %(message)s')
    if verbose == 1:
        level = logging.INFO
    else:
        level = logging.DEBUG

    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(level)
    logging.getLogger().addHandler(stream_handler)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--depth", help="Maximum pattern depth", default=1)
    parser.add_argument("-i", "--input", help="Input RDF graph", default=None)
    parser.add_argument("-t", "--threshold", help="Minimal Frequency threshold", default=1)
    parser.add_argument("-v", "--verbose", help="Increase output verbosity", action='count', default=0)
    args = parser.parse_args()

    init_logger(args.verbose)

    assert is_readable(args.input)

    g = Graph()
    g.parse(args.input, format=guess_format(args.input))

    patterns, support = frequent_pattern_miner(g, int(args.threshold), int(args.depth))

    stdout.write("[support]\t[pattern]\n")
    stdout.writelines([str(support[pattern])+"\t"+str(pattern)+"\n" for
                       pattern in patterns])
