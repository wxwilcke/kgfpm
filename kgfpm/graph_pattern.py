#!/usr/bin/env python

import logging
from uuid import uuid4


class GraphPattern:
    """ Graph pattern

    """
    connections = None
    root = None
    _hash = None

    def __init__(self, connections=None, root=None):
        self.logger = logging.getLogger(__name__)

        self.connections = connections
        self.root = root

        if self.root is None:
            raise Exception()
        if self.connections is None:
            self.connections = {root: set()}

        for pattern_fragment in self.connections.keys():
            for connection in self.connections[pattern_fragment]:
                if connection not in self.connections.keys():
                    self.connections[connection] = set()

        self._hash = hash(self)

    def extend(self, endpoint, extension):
        self.connections[endpoint].add(extension)
        self.connections[extension] = set()
        self._hash = hash(self)

    def copy(self):
        return GraphPattern(connections={k:{v for v in self.connections[k]} for k in self.connections.keys()},
                            root=self.root)

    def __eq__(self, other):
        return isinstance(other, self.__class__)\
                and other._hash == self._hash

    def __ne__(self, other):
        return not self.__eq__(other)

    def _pattern_fragment_str(self, pattern_fragment):
        # create strings by hand to omit variable uuids
        description = pattern_fragment.__class__.__name__
        description += "".join([str(ctype) for ctype in pattern_fragment.domain.class_type])\
           + "".join([str(rtype) for rtype in pattern_fragment.domain.resource_type])\
           + str(pattern_fragment.predicate)\
           + "".join([str(ctype) for ctype in pattern_fragment.range.class_type])\
           + "".join([str(rtype) for rtype in pattern_fragment.range.resource_type])

        return description

    def __hash__(self):
        description = self.__class__.__name__
        for pattern_fragment in sorted(self.connections.keys()):
            description += self._pattern_fragment_str(pattern_fragment)
            for connected_fragment in sorted(list(self.connections[pattern_fragment])):
                description += self._pattern_fragment_str(connected_fragment)

        return hash(description)

    def __len__(self):
        return sum({len(s) for s in self.connections.values()})

    def __str__(self):
        description = "{} -> {{{}}}; ".format(str(self.root),
                                              ", ".join([str(pattern_fragment) for pattern_fragment in self.connections[self.root]]))
        for k in self.connections.keys():
            if k is self.root:
                continue
            description += "{} -> {{{}}}; ".format(str(k),
                                              ", ".join([str(pattern_fragment) for pattern_fragment in self.connections[k]]))

        return description

class TriplePattern:
    """ Context aware wrapper around uninstantiated fact

    param pattern: an uninstantiated fact (varX, predicate, varY)
    """
    domain = None
    range = None
    predicate = None

    def __init__(self, pattern=(None, None, None)):
        self.logger = logging.getLogger(__name__)

        assert len(pattern) == 3
        self.domain = pattern[0]
        self.predicate = pattern[1]
        self.range = pattern[2]

    def copy(self, reset_uuid=False):
        return TriplePattern((self.domain.copy(reset_uuid),
                              self.predicate,
                              self.range.copy(reset_uuid)))

    def __eq__(self, other):
        return isinstance(other, self.__class__)\
                and self.predicate == other.predicate\
                and self.domain == other.domain\
                and self.range == other.range

    def __ne__(self, other):
        return not self.__eq__(other)

    def __lt__(self, other):
        # ignore variable types as order relates to topology
        if self.predicate < other.predicate:
            return True
        if self.domain.uuid < other.domain.uuid\
           or self.range.uuid < other.range.uuid:
            return True
        return False

    def __hash__(self):
        # ignore variable uuids
        return hash(self.__class__.__name__
                    + "".join([str(ctype) for ctype in self.domain.class_type])
                    + "".join([str(rtype) for rtype in self.domain.resource_type])
                    + str(self.predicate)
                    + "".join([str(ctype) for ctype in self.range.class_type])
                    + "".join([str(rtype) for rtype in self.range.resource_type]))

    def __str__(self):
        return "<{}, {}, {}>".format(self.domain,
                                     self.predicate,
                                     self.range)

class TypedVariable:
    """ Atom variable

    Variables are of one or more types or of type rdfs:Resource (default)
    when unrestricted.

    param class_type: a list of URIs representing the various types of this variable
    param resource_type: a list denoting either 'entity' or 'literal', or both
    param consistency: a float between 0 (inconsistent) and 1 (consistent)
    """
    _TYPE_RESOURCE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#Resource"

    uuid = None  # unique identifier to represent unbounded resource
    class_type = None  # rdf:Resource (default) or other type
    resource_type = None  # entity, literal, or both
    consistency = None  # 0 =< v <= 1, else unknown

    def __init__(self,
                 class_type=[_TYPE_RESOURCE],
                 resource_type=[],
                 consistency=-1):
        self.logger = logging.getLogger(__name__)

        self.uuid = uuid4().hex
        self.class_type = sorted(class_type)
        self.resource_type = sorted(resource_type, key=lambda k: k.__name__)
        self.consistency = consistency

    def copy(self, reset_uuid=False):
        # uuids are copied by default
        copy = TypedVariable(self.class_type,
                             self.resource_type,
                             self.consistency)
        if not reset_uuid:
            copy.uuid = self.uuid

        return copy

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash(self.__class__.__name__ + self.uuid
                    + "".join([str(ctype) for ctype in self.class_type])
                    + "".join([str(rtype) for rtype in self.resource_type]))

    def __str__(self):
        return str(self.uuid)\
                + " [" + ", ".join([str(rtype) for rtype in self.resource_type]) + "]"\
                + "(" + "; ".join([str(ctype) for ctype in self.class_type]) + ")"
