#!/usr/bin/env python

from collections import Counter

from rdflib.graph import Literal
from rdflib.namespace import RDF, RDFS, XSD

from kgfpm.graph_pattern import TypedVariable


def build_lookup_dict(g, predicates):
    lookup = dict()
    for lhs, predicate, rhs in g:
        if predicate not in predicates:
            continue

        if predicate not in lookup.keys():
            lookup[predicate] = {'domain': dict(), 'range': dict()}

        if lhs not in lookup[predicate]['domain'].keys():
            lookup[predicate]['domain'][lhs] = {rhs}
        else:
            lookup[predicate]['domain'][lhs].add(rhs)

        if rhs not in lookup[predicate]['range'].keys():
            lookup[predicate]['range'][rhs] = {lhs}
        else:
            lookup[predicate]['range'][rhs].add(lhs)

    return lookup

def mkvariable(g, domain, min_type_contribution):
    # entity or literal
    resource_type_count = Counter({type(vertex) for vertex in domain})
    resource_type_count_sum = sum(resource_type_count.values())
    resource_types = [rtype for rtype,count in resource_type_count.items()
                       if count/resource_type_count_sum >= min_type_contribution]
    # class or data type
    # TODO: extend with rdfs closure (subtypes)
    class_type_count = Counter(_types_of(g, domain))
    class_type_count_sum = sum(class_type_count.values())
    class_types = [ctype for ctype,count in class_type_count.items()
                       if count/class_type_count_sum >= min_type_contribution]

    consistency = _consistency(resource_type_count, class_type_count)

    return TypedVariable(class_types, resource_types, consistency)

def _consistency(resource_type_count, class_type_count, sensitivity=0.0):
    # very inconsistent (0) - very consistent (1)
    return 0.5*_partial_consistency(resource_type_count, sensitivity)\
            + 0.5*_partial_consistency(class_type_count, sensitivity)

def _partial_consistency(count, sensitivity=0.0):
    # (dV - s*dV) / sum(V)
    v = count.values()
    if len(v) <= 1:
        return 1.0

    delta = max(v)-min(v)
    return (delta-sensitivity*delta)/sum(v)

def _types_of(g, domain):
    types = list()
    for vertex in domain:
        if type(vertex) is Literal:
            datatype = vertex.datatype
            if datatype is None:
                types.append(XSD.string if type(vertex.value) is str else None)
            else:
                types.append(datatype)
        else:  # if entity
            class_types = list(g.objects(vertex, RDF.type))
            if len(class_types) <= 0:
                # if none specified, assume any type (RDFS:Resource)
                # this get filtered out by min class coverage if this is an
                # error an other vertices do specify classes
                types.append(RDFS.Resource)
            else:
                types.extend(class_types)
    return types
