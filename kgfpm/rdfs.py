#!/usr/bin/env python

import logging

from rdflib import Graph
from rdflib.namespace import OWL, RDF, RDFS


class RDFSClosure:
    """ Compute RDFS class closure
    """
    _graph = None
    _map = None

    def __init__(self, graph):
        if type(graph) is not Graph:
            raise TypeError("Expected {}, got {} instead".format(Graph,
                                                                 type(graph)))
        self._graph = graph
        self._map = dict()

    def subClassOf(self, clss, superclass):
        # is clss a subclass of superclass?
        return superclass in self._map[clss]

    def compute(self):
        logging.info("Computing RDFS closure")
        for clss in self._graph.subjects(RDF.type, RDFS.Class):
            if clss not in self._map.keys():
                self._map[clss] = set(self._graph.objects(clss, RDFS.subClassOf))
        for clss in self._graph.subjects(RDF.type, OWL.Class):
            if clss not in self._map.keys():
                self._map[clss] = set(self._graph.objects(clss, RDFS.subClassOf))

        closed = set()
        for clss in self._map.keys():
            self._map[clss] |= self._compute_class_closure(clss, closed)

    def _compute_class_closure(self, clss, closed=None, _class_tree=None):
        if clss in closed:
            return self._map[clss]
        if clss not in self._map.keys():
            return {clss}
        if _class_tree is None:
            _class_tree = set()

        for superclass in self._map[clss]:
            _class_tree |= self._compute_class_closure(superclass, closed, _class_tree)

        _class_tree.add(clss)

        self._map[clss] |= _class_tree
        closed.add(clss)

        return _class_tree
