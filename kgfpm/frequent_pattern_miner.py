#!/usr/bin/env python

import logging
from collections import Counter

from rdflib.graph import Literal

from kgfpm.graph_pattern import GraphPattern, TriplePattern
from kgfpm.utils import build_lookup_dict, mkvariable

# an extension to x, y (eg, a->b->c->d), always has equal or lower
# coverage to its parent x (eg a->b->c) because it is more specific
# so, if x is below coverage, so is y and further extensions, thus that branch
# may be skipped (anti-monotonic property)

def frequent_pattern_miner(g, min_frequency=1, max_depth=1):
    # generate all frequent patterns of length == 1
    logging.debug("Generating frequent patterns of length == 1")
    triple_patterns = _frequent_triple_patterns(g, min_frequency)

    # generate lookup dictionary to prevent many repeated graph queries
    logging.debug("Generating Lookup Dictionary")
    lookup = build_lookup_dict(g, {triple_pattern.predicate for
                                       triple_pattern in triple_patterns})

    # generate all frequent patterns with 1 < length <= max_depth
    logging.debug("Generating frequent patterns with 1 < length <= {}".format(max_depth))
    patterns = set()
    patterns_support = dict()
    for triple_pattern in triple_patterns:
        triple_pattern_copy = triple_pattern.copy()
        graph_pattern = GraphPattern(root=triple_pattern_copy)

        logging.debug("Exploring pattern tree with root <{}>".format(str(triple_pattern.predicate)))
        patterns_update, patterns_support_update = _pattern_extension(lookup,
                                                                      graph_pattern,
                                                                      triple_patterns,
                                                                      min_frequency,
                                                                      max_depth)
        patterns |= patterns_update
        patterns_support.update(patterns_support_update)

        # why should this be needed?
        #KG.Rmv(set(kg.facts(None, triple_pattern[1], None)))

        # don't rm the pattern from the set because we are working with directed
        # instead of undirected graphs, meaning that a->b != b->a
        #triple_patterns.discard(triple_pattern)

    return (patterns, patterns_support)

def _pattern_extension(lookup,
                       graph_pattern,
                       pattern_extensions,
                       min_frequency,
                       max_depth,
                       _depth=1):
    results = {graph_pattern}
    pattern_domain = set(lookup[graph_pattern.root.predicate]['domain'].keys())
    support, _ = _mni_support(lookup, graph_pattern, graph_pattern.root, pattern_domain, min_frequency)
    results_support = {graph_pattern: support}

    if _depth >= max_depth:
        return (results, results_support)

    candidates = set()
    candidates_hashed = set()
    for pattern_extension in pattern_extensions:
        # domain of extension; always entity classes
        ext_ctype = frozenset(pattern_extension.domain.class_type)

        for pattern_fragment in graph_pattern.connections.keys():
            # skip if only literal range
            frag_rtype = pattern_fragment.range.resource_type
            if len(frag_rtype) <= 1 and Literal in frag_rtype:
                # terminal node
                continue

            # range of fragment
            frag_ctype = frozenset(pattern_fragment.range.class_type)
            common_types = ext_ctype & frag_ctype  # intersection
            if len(common_types) <= 0:
                # datatypes are filtered out because ext_ctype (domain) always
                # holds entity classes
                continue

            # add extension to pattern
            graph_pattern_copy = graph_pattern.copy()
            graph_pattern_copy.extend(endpoint=pattern_fragment,
                                      extension=pattern_extension)

            # add new pattern if not already stored
            if graph_pattern_copy._hash not in candidates_hashed:
                candidates.add(graph_pattern_copy)
                candidates_hashed.add(graph_pattern_copy._hash)

    for candidate in candidates:
        is_frequent, support = _is_freq_pattern(lookup, candidate, min_frequency)

        if is_frequent:
            results_update, results_support_update = _pattern_extension(lookup,
                                                                        candidate,
                                                                        pattern_extensions,
                                                                        min_frequency,
                                                                        max_depth,
                                                                        _depth+1)
            results |= results_update
            results_support.update(results_support_update)

    return (results, results_support)

def _is_freq_pattern(lookup, graph_pattern, min_frequency):
    """ A pattern is frequent if its minimum image-based support (MNI) equals or
    exceeds the threshold. MNI := number of instantiations

    CSP:
        X := set of placeholder vertices with unique uuids
        D := set of vertices in G that match type of x in X
        C := 1) D(x0) != D(x1) for x0,x1 in X (no cycles, simplification)
             2) L(e(x0,x1)) == L(e(D(x0),D(x1))) (same predicates)
    """
    root_domain = set(lookup[graph_pattern.root.predicate]['domain'].keys())
    support, _ = _mni_support(lookup, graph_pattern, graph_pattern.root, root_domain, min_frequency)

    return (support >= min_frequency, support)

def _mni_support(lookup,
                 graph_pattern,
                 pattern_fragment,
                 pattern_fragment_domain,
                 min_frequency):
    """ Calculate Minimal Image-Based Support for a Graph Pattern

    Returns -1 if support < min_frequency

    Optimized to minimalize the work done by continuously reducing the search
    space and by early stopping when possible
    """
    # no need to continue if we are a terminal node
    if len(graph_pattern.connections[pattern_fragment]) <= 0:
        support = 0
        for resource in pattern_fragment_domain:
            support += len(lookup[pattern_fragment.predicate]['domain'][resource])

        return (support, pattern_fragment_domain)

    # retrieve range based on fragment's domain
    pattern_fragment_range = set()
    for resource in pattern_fragment_domain:
        pattern_fragment_range |= lookup[pattern_fragment.predicate]['domain'][resource]

    # update range based on connected fragments' domains
    for connected_fragment in graph_pattern.connections[pattern_fragment]:
        if len(pattern_fragment_range) < min_frequency:
            return (-1, set())

        pattern_fragment_range &= frozenset(lookup[connected_fragment.predicate]['domain'].keys())

    # update range based on connected fragments' returned updated domains
    # search space is reduced after each returned update
    connected_fragment_domain = pattern_fragment_range  # only for readability
    for connected_fragment in graph_pattern.connections[pattern_fragment]:
        if len(pattern_fragment_range) < min_frequency:
            return (-1, set())

        support, range_update = _mni_support(lookup,
                                             graph_pattern,
                                             connected_fragment,
                                             connected_fragment_domain,
                                             min_frequency)
        if support < min_frequency:
            return (-1, set())

        pattern_fragment_range &= range_update

    # update domain based on updated range
    support = 0
    pattern_fragment_domain_updated = set()
    for resource in pattern_fragment_range:
        domain_update = lookup[pattern_fragment.predicate]['range'][resource]
        pattern_fragment_domain_updated |= domain_update

        support += len(domain_update)

    return (support, pattern_fragment_domain_updated)

def _frequent_triple_patterns(g, min_frequency, min_consistency=0.2, min_type_contribution=0.2):
    predicate_count = Counter(g.predicates())
    freq_predicates = {predicate for predicate, freq in predicate_count.items()
                       if freq >= min_frequency}
    # add ignore list? eg labels, inv links

    freq_triple_patterns = set()
    for predicate in freq_predicates:
        # lhs
        domain_lhs = list(g.subjects(predicate, None))
        tvar_lhs = mkvariable(g, domain_lhs, min_type_contribution)
        if tvar_lhs.consistency < min_consistency:
            # too high variance between types
            continue

        # rhs
        domain_rhs = list(g.objects(None, predicate))
        tvar_rhs = mkvariable(g, domain_rhs, min_type_contribution)
        if tvar_rhs.consistency < min_consistency:
            # too high variance between types
            continue

        freq_triple_patterns.add(TriplePattern((tvar_lhs, predicate, tvar_rhs)))

    return freq_triple_patterns
